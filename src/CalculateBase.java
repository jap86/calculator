//Is de basisklasse waarvan gespecialiseerde klasse erven. Doet zelfde als MathEquation, maar overzichtelijker
//en dus makkellijker te onderhouden.
// Is abstract, omdat er een abstracte methode in zit.
public abstract class CalculateBase {
//Waarde benoemen die we nodig hebben, zijn prive omdat we niet willen dat ze veranderd kunnen worden.
//Daarom moeten we ook methodes benoemen om de waarde te vekrijgen en te benoemen.
    private double leftVal;
    private double rightVal;
    private double result;

    public double getLeftVal() {return leftVal;}
    public void setLeftVal (double leftVal) {this.leftVal=leftVal;}
    public double getRightVal () {return rightVal;}
    public void setRightVal (double rightVal) {this.rightVal = rightVal;}
    public double getResult () {return result;}
    public void setResult (double result) {this.result = result;}

    //Constructors, een lege als 1 die de twee waarden ontvangt
    public CalculateBase (){}
    public CalculateBase (double leftVal, double rightVal){
        this.leftVal = leftVal;
        this.rightVal = rightVal;
    }


    //Abstracte methode die berekent. Is abstract omdat dochterklasses ieder iets anders moet doen met die methode.
    public abstract void calculate ();
}
