public class CalculateHelper {

private static final char ADD_SYMBOL = '+';
private static final char SUBSTRACT_SYMBOL = '-';
private static final char MULTIPLY_SYMBOL = '*';
private static final char DIVIDE_SYMBOL = '/';
    Mathcommand command;
    double leftValue;
    double rightValue;
    double result;

    public void process(String statement) throws InvalidStatementException{
        //input is iets als add 3.0 5.0 We willen die string splitten en omzetten naar doubles and de command
        String[] parts = statement.split(" ");//Split statement waar een spatie is en slaat delen op in array parts
        if(parts.length != 3)
            throw new InvalidStatementException("Incorrect number of fields", statement);
        String commandString = parts[0]; //Eerste deel van Statement moet het commando zijn, dat wordt dus weer een string

        try {
            leftValue = Double.parseDouble(parts[1]);//leftValue = 2e deel van array parts omgezet in een double
            rightValue = Double.parseDouble(parts[2]);//rightValue = 2e deel van array parts omgezet in een double
        } catch (NumberFormatException e) {
throw new InvalidStatementException("Non-numeric data", statement, e);
        }

        setCommandFromString(commandString);
        if(command == null)
            throw new InvalidStatementException("Invalid command", statement);

        CalculateBase calculator = null;
        switch (command) {  //Maakt een adder,substracter van de command.
            case Add:
                calculator = new Adder(leftValue, rightValue);
                break;
            case Substract:
                calculator = new Substracter(leftValue,rightValue);
                break;
            case Multiply:
                calculator = new Multiplier(leftValue, rightValue);
                break;
            case Divide:
                calculator = new Divider(leftValue, rightValue);
                break;
        }

        calculator.calculate();
        result = calculator.getResult();
    }

    private void setCommandFromString(String commmandString)
    //Moet de sting waarde (commandstring) omzetten in de enum waarde van Mathcommand
    {


        if (commmandString.equalsIgnoreCase(Mathcommand.Add.toString()))
            command = Mathcommand.Add;
        else if (commmandString.equalsIgnoreCase(Mathcommand.Substract.toString())) //ignore case betekend ignore upper of lowercase.
                                                                                    // to String betekend dat hij de enumuration waarde naar een string omzet
        command = Mathcommand.Substract;  //command wordt mathcommand.Substract
        else if (commmandString.equalsIgnoreCase(Mathcommand.Multiply.toString()))
        command = Mathcommand.Multiply;
        else if (commmandString.equalsIgnoreCase(Mathcommand.Divide.toString()))
        command = Mathcommand.Divide;
    }
    @Override
public String toString() {
//zorgt ervoor dat de string wordt uitgeprint en niet de locatie van het object
        char symbol = ' ';
        switch (command){
            case Add: symbol = ADD_SYMBOL;
            break;
            case Substract:symbol = SUBSTRACT_SYMBOL;
            break;
            case Multiply:symbol = MULTIPLY_SYMBOL;
            break;
            case Divide:symbol = DIVIDE_SYMBOL;
            break;
        }

        StringBuilder sb = new StringBuilder(20);
        sb.append(leftValue);
        sb.append(' ');
        sb.append(symbol);
        sb.append(' ');
        sb.append(rightValue);
        sb.append(' ');
        sb.append('=');
        sb.append(' ');
        sb.append(result);

        return sb.toString();



}
}

