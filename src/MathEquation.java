public class MathEquation {

// initial fields
        private double leftVal;
        private double rightVal;
        private char opCode ;
        private double result;

      //constructors
      // Zorgt ervoor dat je een instance kan maken met de operatie, zonder dat je nog de waarde weet.
     public MathEquation(char opCode){
         this.opCode=opCode;
     }
     // Constructor die op eerste regel verwijst naar andere constructor, en de opcode en de waardes als input neemt
    // dus als je een intance maakt van de klasse, kan je de waarde gelijk opgeven.
    public MathEquation(char opCode, double leftVal, double rightVal){
         this(opCode);
         this.leftVal = leftVal;
         this.rightVal = rightVal;
     }

    public MathEquation() {

    }

    //methodes om de waardes te verkrijgen en veranderen. Behalve van return, daar alleen vekrijgen.
        public double getLeftVal() {return leftVal;}
        public void setLeftVal (double leftVal){this.leftVal = leftVal;}
        public double getRightVal () {return rightVal;}
        public void setRightVal (double rightVal){this.rightVal = rightVal;}
        public char getOpCode() {return opCode;}
        public void setOpCode(char opCode) {this.opCode = opCode;}
        public double getResult() {return result;}

        //methode die leftVal en Rightval als parameters neemt en vervolgens doorstuurt naar void execute
    public void execute(double leftVal, double rightVal){
         this.leftVal = leftVal;
         this.rightVal = rightVal;
         execute();
    }
    public void execute(int leftVal, int rightVal) {
        this.leftVal = leftVal;
        this.rightVal = rightVal;
        execute();
        result = (int) result;
    }
    // methode die de operatie uitvoert voor de arrays)
        public void execute() {
            switch (opCode) {
                case 'a':
                    result = leftVal + rightVal;
                    break;
                case 's':
                    result = leftVal - rightVal;
                    break;
                case 'd':
                    result = rightVal != 0.0d ? leftVal / rightVal : 0.0d;
                    break;
                case 'm':
                    result = leftVal * rightVal;
                    break;
                default:
                    System.out.println("Error - invalid opCode");
                    result = 0.0d;
                    break;
            }
        }

        }


    /* public double leftVal;
    public double rightVal;
    public char opCode;
    public double result;

public void execute(){
        switch (opCode){
            case 'a':
                result  = leftVal + rightVal;
                break;
            case 's' :
                result = leftVal - rightVal;
                break;
            case 'd' :
                result = leftVal != 0.0d ? leftVal/ rightVal: 0.0d;
                break;
            case 'm' :
                result = leftVal * rightVal;
                break;
            default :
                System.out.println("Error - wrong opCode");
                result = 0.0d;
                break;}

}*/


