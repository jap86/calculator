public class DynamicHelper {
    private MathProcessing [] handlers;

    public DynamicHelper (MathProcessing [] handlers) {
        this.handlers = handlers;
    }

    public String process(String statement) {
        //IN: add 1.0 2.0
        //OUT: 1.0 + 2.0 = 3.0

        String[] parts = statement.split(MathProcessing.SEPARATOR);
        String keyword = parts[0]; //add

        MathProcessing theHandler = new MathProcessing() {
            @Override
            public String getKeyword() {
                return null;
    }

    @Override
    public char getSymbol() {
        return 0;
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {
        return 0;
    }
};

        for(MathProcessing handler:handlers) {
                if(keyword.equalsIgnoreCase(handler.getKeyword())) {
                theHandler = handler;
                }
                break;
                }

        double leftVal = Double.parseDouble(parts[1]); //1.0
        double rightVal = Double.parseDouble(parts[2]); //2.0


        double result = theHandler.doCalculation(leftVal, rightVal);


        StringBuilder sb = new StringBuilder(20);
        sb.append(leftVal);
        sb.append(' ');
        sb.append(theHandler.getSymbol());
        sb.append(' ');
        sb.append(rightVal);
        sb.append(' ');
        sb.append('=');
        sb.append(' ');
        sb.append(result);

        return sb.toString();
    }
}
